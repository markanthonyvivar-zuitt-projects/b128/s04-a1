INSERT INTO users (email,password,date_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");

INSERT INTO users (email,password,date_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");

INSERT INTO users (email,password,date_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");

INSERT INTO users (email,password,date_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");

INSERT INTO users (email,password,date_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");




INSERT INTO posts (title,content,date_created) VALUES ("First Code", "Hello World!", "2021-01-01 01:00:00");

INSERT INTO posts (title,content,date_created) VALUES ("Second Code", "Hello Earth!", "2021-01-01 02:00:00");

INSERT INTO posts (title,content,date_created) VALUES ("Third Code", "Welcome To Mars!", "2021-01-01 03:00:00");

INSERT INTO posts (title,content,date_created) VALUES ("Fourth Code", "Bye bye Solar System", "2021-01-01 04:00:00");


SELECT <column>, [<column>, …] FROM <table_name> WHERE <condition>;

SELECT id,title,content,date_created FROM posts WHERE id = 1;

SELECT email,date_created FROM users;

UPDATE <table_name> SET <column> = <new_value> WHERE <condition>;

UPDATE posts SET content = "Hello to the people of the Earth" WHERE content = "Hello Earth!";


DELETE FROM users WHERE email = "johndoe@gmail.com";




